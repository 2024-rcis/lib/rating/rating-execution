package de.ubt.ai4.petter.recpro.lib.rating.ratingexecution.service;

import de.ubt.ai4.petter.recpro.lib.rating.ratingexecution.model.RatingDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/recpro/rating/execution")
@AllArgsConstructor
public class RatingExecutionController {

    private RatingExecutionService ratingExecutionService;

    @GetMapping("init")
    public ResponseEntity<RatingDto> init(@RequestParam String bpmElementId, @RequestParam String elementInstanceId, @RequestParam String processInstanceId,  @RequestHeader("X-User-ID") String userId) {
        return ResponseEntity.ok(ratingExecutionService.init(bpmElementId, elementInstanceId, processInstanceId, userId));
    }

}
