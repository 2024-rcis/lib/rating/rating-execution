package de.ubt.ai4.petter.recpro.lib.rating.ratingexecution.service;

import de.ubt.ai4.petter.recpro.lib.ontology.modeling.rating.RatingModelingService;
import de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model.*;
import de.ubt.ai4.petter.recpro.lib.rating.ratingexecution.model.RatingDto;
import de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence.model.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;



@Service
@AllArgsConstructor
public class RatingExecutionService {
    private final RatingModelingService ratingModelingService;

    public RatingDto init(String bpmElementId, String elementInstanceId, String processInstanceId, String userId) {
        Rating rating = ratingModelingService.getByBpmElementId(bpmElementId);
        RatingInstance ratingInstance;

        switch (rating.getRatingType()) {
            case BINARY -> ratingInstance = new BinaryRatingInstance(-1L, bpmElementId, elementInstanceId, rating.getId(), rating.getRatingType(), processInstanceId, ((BinaryRating) rating).isDefaultValue(), userId);
            case CONTINUOUS -> ratingInstance = new ContinuousRatingInstance(-1L, bpmElementId, elementInstanceId, rating.getId(), rating.getRatingType(), processInstanceId, ((ContinuousRating) rating).getDefaultValue(), userId);
            case ORDINAL -> ratingInstance = new OrdinalRatingInstance(-1L, bpmElementId, elementInstanceId, rating.getId(), rating.getRatingType(), processInstanceId, ((OrdinalRating) rating).getDefaultValue(), userId);
            case UNARY -> ratingInstance = new UnaryRatingInstance(-1L, bpmElementId, elementInstanceId, rating.getId(), rating.getRatingType(), processInstanceId, ((UnaryRating) rating).isDefaultValue(), userId);
            case INTERVAL_BASED -> ratingInstance = new IntervalBasedRatingInstance(-1L, bpmElementId, elementInstanceId, rating.getId(), rating.getRatingType(), processInstanceId, ((IntervalBasedRating) rating).getDefaultValue(), userId);
            default -> ratingInstance = new RatingInstance(-1L, bpmElementId, elementInstanceId, rating.getId(), rating.getRatingType(), processInstanceId, userId);
        }

        return new RatingDto(rating, ratingInstance);
    }
}
